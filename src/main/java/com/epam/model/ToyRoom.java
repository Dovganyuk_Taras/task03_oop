package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ToyRoom {
    private static Logger logger = LogManager.getLogger(Application.class);
    private List<Toy> toyList = new LinkedList<>();
    private int totalAmount;
    private AgeGroup ageGroup;

    public ToyRoom(int totalAmount, AgeGroup ageGroup) {
        this.totalAmount = totalAmount;
        this.ageGroup = ageGroup;
    }

    public void generateToyList(List<Toy> fullToyList) {
        List<Toy> toyListForAgeGroup = new ArrayList<>();
//        System.out.println("Full toy list:");
        logger.debug("\nFull toy list:");
          for (Toy t:fullToyList) {
              logger.debug(t);
//            System.out.println(t);
//           t.play();
           if (t.getAgeGroup().equals(ageGroup)) {
               toyListForAgeGroup.add(t);
           }
        }


//        System.out.println("\nOur toy list for age group " + ageGroup + ":");
        logger.debug("\nOur toy list for age group " + ageGroup + ":");
        for (Toy t:toyListForAgeGroup) {
            logger.debug(t);
//            System.out.println(t);
        }

        int minPrice = toyListForAgeGroup.get(0).getPrice();
        for (Toy t:toyListForAgeGroup) {
            if (t.getPrice() < minPrice) {
                minPrice = t.getPrice();
            }
        }

        int currAmount = 0;
        Random random = new Random();
        while ((totalAmount - currAmount >= minPrice) && !toyListForAgeGroup.isEmpty()) {
            int idx = random.nextInt(toyListForAgeGroup.size());
            Toy randomToy = toyListForAgeGroup.get(idx);
            if (totalAmount - currAmount >= randomToy.getPrice()) {
                toyList.add(randomToy);
                toyListForAgeGroup.remove(idx);
                currAmount += randomToy.getPrice();
                if (!toyListForAgeGroup.isEmpty()) {
                    minPrice = toyListForAgeGroup.get(0).getPrice();
                }
                 for (Toy t:toyListForAgeGroup) {
                    if (t.getPrice() < minPrice) {
                        minPrice = t.getPrice();
                    }
                }
            }
        }

        logger.info("\nOur toy ROOM for age group " + ageGroup + " for price " + totalAmount +" paid:");
        //System.out.println("\nOur toy ROOM for age group " + ageGroup + " for price " + totalAmount +" paid:");
        for (Toy t:toyList) {
            logger.info(t);
           // System.out.println(t);

        }

       // fullToyList.sort(Comparator.comparing());
    }
}
