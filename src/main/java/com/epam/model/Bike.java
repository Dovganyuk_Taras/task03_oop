package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Bike extends Toy implements Movable{
    private int speed;
    private static Logger logger = LogManager.getLogger(Application.class);

    public Bike(ToySize size, AgeGroup ageGroup, int price, int speed) {
        super(size, ageGroup, price);
        this.speed = speed;
    }

    @Override
    public void move() {
//        System.out.println("Bike moving with speed: " + speed);
        logger.info("Bike moving with speed: " + speed);

    }

    @Override
    public void play() {
//        System.out.println("Playing with Bike");
        logger.info("Playing with Bike");
    }
}
