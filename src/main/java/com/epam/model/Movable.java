package com.epam.model;

public interface Movable {
    void move();
}
