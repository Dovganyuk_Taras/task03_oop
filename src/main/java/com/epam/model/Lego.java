package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Lego extends Toy implements Constructable {
    private int countPieces;
    private static Logger logger = LogManager.getLogger(Application.class);

    public Lego(ToySize size, AgeGroup ageGroup, int price, int countPieces) {
        super(size, ageGroup, price);
        this.countPieces = countPieces;
    }

    @Override
    public void play() {
//        System.out.println("Playing with " + countPieces + " pieces of lego");
        logger.info("Playing with " + countPieces + " pieces of lego");
    }

    public void build() {
//        System.out.println("Constructing lego");
        logger.info("Constructing lego");
    }
}
