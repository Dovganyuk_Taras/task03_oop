package com.epam.model;

public enum AgeGroup {
    LOW,
    MEDIUM,
    HIGH
}
