package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cube extends Toy implements Constructable {
    private int countCubes;
    private static Logger logger = LogManager.getLogger(Application.class);

    public Cube(ToySize size, AgeGroup ageGroup, int price, int countCubes) {
        super(size, ageGroup, price);
        this.countCubes = countCubes;
    }

    @Override
    public void play() {
//        System.out.println("Playing with " + countCubes + " cubes");
        logger.info("Playing with " + countCubes + " cubes");
    }

    public void build() {
//        System.out.println("Constructing cubes");
        logger.info("Constructing cubes");

    }
}
