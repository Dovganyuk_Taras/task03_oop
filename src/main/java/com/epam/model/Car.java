package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Car extends Toy implements Movable {
    private int speed;
    private static Logger logger = LogManager.getLogger(Application.class);

    public Car(ToySize size, AgeGroup ageGroup, int price, int speed) {
        super(size, ageGroup, price);
        this.speed = speed;
    }

    @Override
    public void play() {
//        System.out.println("Playing with car");
        logger.info("Playing with car");

    }

    public void move() {
//        System.out.println("Car moving with speed: " + speed);
        logger.info("Car moving with speed: " + speed);
    }
}
