package com.epam.model;

public enum ToySize {
    SMALL,
    MIDDLE,
    BIG
}
