package com.epam.model;

public interface Constructable {
    void build();
}
