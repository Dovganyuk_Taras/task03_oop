package com.epam.model;

public abstract class Toy {
    private ToySize size;
    private AgeGroup ageGroup;
    private int price;

    public abstract void play();

    public Toy( ToySize size, AgeGroup ageGroup, int price) {
        this.size = size;
        this.ageGroup = ageGroup;
        this.price = price;
    }

    public ToySize getSize() {
        return size;
    }

    public void setSize(ToySize size) {
        this.size = size;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Toy{" +
                "size=" + size +
                ", ageGroup=" + ageGroup +
                ", price=" + price +
                '}';
    }
}