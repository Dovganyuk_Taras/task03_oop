package com.epam.controller;

import com.epam.model.*;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    private ToyRoom toyRoom;
    private List<Toy> toys = new ArrayList<Toy>();

    public Controller() {
        toys.add(new Cube(ToySize.BIG, AgeGroup.LOW, 5, 5));
        toys.add(new Cube(ToySize.MIDDLE, AgeGroup.HIGH, 20, 15));
        toys.add(new Cube(ToySize.SMALL, AgeGroup.MEDIUM, 12, 10));
        toys.add(new Cube(ToySize.SMALL, AgeGroup.HIGH, 25, 20));
        toys.add(new Cube(ToySize.MIDDLE, AgeGroup.MEDIUM, 15, 12));
        toys.add(new Lego(ToySize.BIG, AgeGroup.HIGH, 40, 100));
        toys.add(new Lego(ToySize.MIDDLE, AgeGroup.MEDIUM, 20, 48));
        toys.add(new Lego(ToySize.SMALL, AgeGroup.LOW, 16, 36));
        toys.add(new Lego(ToySize.SMALL, AgeGroup.MEDIUM, 25, 84));
        toys.add(new Lego(ToySize.MIDDLE, AgeGroup.LOW, 10, 20));
        toys.add(new Car(ToySize.SMALL, AgeGroup.LOW, 1, 2));
        toys.add(new Car(ToySize.BIG, AgeGroup.HIGH, 10, 10));
        toys.add(new Car(ToySize.SMALL, AgeGroup.MEDIUM, 50, 4));
        toys.add(new Car(ToySize.MIDDLE, AgeGroup.HIGH, 33, 20));
        toys.add(new Car(ToySize.BIG, AgeGroup.MEDIUM, 24, 12));
        toys.add(new Bike(ToySize.MIDDLE, AgeGroup.LOW, 3, 3));
        toys.add(new Bike(ToySize.SMALL, AgeGroup.LOW, 13, 6));
        toys.add(new Bike(ToySize.BIG, AgeGroup.HIGH, 15, 4));
        toys.add(new Bike(ToySize.BIG, AgeGroup.MEDIUM, 8, 8));
        toys.add(new Bike(ToySize.MIDDLE, AgeGroup.LOW, 4, 5));

    }

    public void generateRoom(final int sum, final AgeGroup ag) {
        toyRoom = new ToyRoom(sum, ag);
        toyRoom.generateToyList(toys);
    }
}
