package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.model.AgeGroup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(Application.class);

    private Controller controller = new Controller();
    private static Scanner input = new Scanner(System.in);

    public void show() {
        int sum = 0;
        int age = 0;
        try {

            logger.info("Enter sum of money:");
            //System.out.println("Enter sum of money:");
            sum = input.nextInt();
            logger.info("Sum entered: " + sum);
            logger.info("Enter age of child from 2 to 14: ");
            //System.out.println("Enter age of child from 2 to 14:");
            age = input.nextInt();
            logger.info("Age entered: " + age);
        } catch (Exception e) {
            logger.warn("Invalid format", e);
            //System.out.println("Invalid format");
            return;
        }

        AgeGroup ag;
        if (age >= 2 && age <= 6) {
            ag = AgeGroup.LOW;
        } else if ((age > 6 && age <= 10)) {
            ag = AgeGroup.MEDIUM;
        } else if ((age > 10 && age <= 14)) {
            ag = AgeGroup.MEDIUM;
        } else {
            logger.info("No rooms available for age "  + age);
            //System.out.println("No rooms available for age "  + age);
            return;
        }

        controller.generateRoom(sum, ag);
    }
}
